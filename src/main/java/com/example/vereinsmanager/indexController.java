package com.example.vereinsmanager;

import com.example.vereinsmanager.sportbeitrag.Sportbeitrag;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class indexController {

// "@RequestParam" um von client side values zu übermitteln
//    @GetMapping("/")
//    @ResponseBody
//    public String index(@RequestParam String name){
//        return "Das ist die Homeseite von " + name;
//    }

    @GetMapping("/")
    public String index(Model model){
        String jvHello = "Hallo Welt!";
        model.addAttribute("attrNameHtml", jvHello);       // Thymeleaf:  jv <-Model für Datenübertragung-> Html
        return "index";
    }

    @GetMapping("/sportbeitraege")
    public String  sportFormular(Model model){
        model.addAttribute("sportFormular", new Sportbeitrag());

        return "sportNews";
    }

    @PostMapping("/sportbeitraege")
    public String sportEintragSubmit(@ModelAttribute Sportbeitrag jvSportbeitrag, Model model){
        model.addAttribute("sportFormular1", jvSportbeitrag);
        return "index";
    }
}
